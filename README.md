***Jupyter-notebook* Tutorials zur Datenauswertung**  

> **(Moderne) Fehlerrechnung in den physikalischen Praktika**

**Günter Quast**, März 2020, (zuletzt aktualisiert Aug. 2024)

Jupyter-notebook Tutorials zu Statistik und Datenauswertung
-----------------------------------------------------------

Einige einfache Beispiele und Tutorials stehen zum Download bereit. Am
besten speichern Sie sie lokal auf Ihrem Computer in einem eigenen
Arbeitsverzeichnis; nutzen Sie dazu die Funktion *\"Ziel speichern
unter ..."* Ihres Browsers (i.a. durch Rechtsklick auf den Link zur
Datei).

Einige Jupyter-notebooks zur Einführung in *Juypter*, *Python* und
Anwendungen zur Datenauswertung sind hier zusammengestellt:

1.  Spickzettel für *Jupyter* :
    [JupyterCheatsheet.ipynb](JupyterCheatsheet.ipynb)
2.  Einführung in *python* : [PythonIntro.ipynb](PythonIntro.ipynb)
3.  Spickzettel für *python* :
    [PythonCheatsheet.ipynb](PythonCheatsheet.ipynb)
4.  Grundlagen zur Verwendung von *matplotlib* :
    [matplotlibTutorial.ipynb](matplotlibTutorial.ipynb)
5.  Grundlagen der Statistik:
    [IntroStatistik.ipynb](IntroStatistik.ipynb)
6.  Fehlerrechnung im Physikalischen Praktikum:
    [Fehlerrechnung.ipynb](Fehlerrechnung.ipynb)
7.  Tutorium zu *kafe2*: [kafe2Tutorial.ipynb](kafe2Tutorial.ipynb)
8.  Modellanpassung für Fortgeschrittene (engl.):
    [advancedFitting.ipynb](advancedFitting.ipynb)
9.  Grundlagen der Multivariaten Datenauswertung mit Hilfe von Maschinellem Lernen:
    [MVA-Grundlagen.ipynb](MVA-Grundlagen.ipynb)

Starten Sie einen *Jupyter*-Server bzw. verbinden Sie sich über Ihren
Web-Browser mit dem [*Jupyter*-Server der Fakultät](https://jupytermachine.etp.kit.edu/).

Wenn die *Jupyter*-Oberfläche in Ihrem Web-Browser erscheint, kann es
direkt los gehen. Sie sehen eine Menüleiste, darunter links ein Fenster
mit einer Dateiliste des Arbeitsverzeichnisses, und rechts ein großes
Fenster für die Anzeige von Text, Programm-Code und Programmausgaben.

Links oben in der Menüleiste gibt es ein Symbol "↥ (Upload)" für das
(Hoch-)Laden eines *Jupyter*-notebooks in den Arbeitsbereich des
*Jupyter*-Servers. Klicken Sie es an und wählen Sie im Datei-Browser
eine der eben heruntergeladenen Dateien des Typs *.ipynb* aus. Das
ausgewählte Notebook erscheint nun in der Dateiliste; starten Sie es mit
einem Doppelklick.

Sie sehen nun den Inhalt des ausgewählten Notebooks im Hauptfenster der
Oberfläche. Folgen Sie den Hinweisen im angezeigten Text; die Tutorials
sollten selbsterklärend sein.


**Weitere Links:**

-   [Skript: Datenauswertung in Grundlagenpraktika zur 
    Physik](http://etp.kit.edu/~quast/Skripte/Datenauswertung.pdf)
-   [Skript: Funktionsanpassung mit der
    $\chi^{2}$-Methode](http://etp.kit.edu/~quast/Skripte/Chi2Method.pdf)
-   Software-Tools im Paket *PhyPraKit*  
     -  [Dokumentation](https://phyprakit.readthedocs.io/en/latest/)   
     -  [Download](https://gitlab.kit.edu/kit/etp-lehre/phyprakit) des gesamten Pakets
        mit Beispielen (↧ - Symbol anklicken)
-   Funktionsanpassung in Python (Paket *kafe2*)  
    -   [Dokumentation](https://kafe2.readthedocs.io/)
    -   [*kafe2* auf github](https://github.com/PhiLFitters/kafe2)
